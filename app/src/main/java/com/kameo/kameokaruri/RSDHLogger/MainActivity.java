package com.kameo.kameokaruri.RSDHLogger;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;


public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    @RequiresApi(api = Build.VERSION_CODES.M)
    /** ブロードキャストマネージャ */
    private LocalBroadcastManager accLBM;
    private BroadcastReceiver mAccReceiver;

    private LocalBroadcastManager bcnLBM;
    private BroadcastReceiver mBcnReceiver;

    private LocalBroadcastManager stpLBM;
    private BroadcastReceiver mStpReceiver;

    private LocalBroadcastManager micLBM;
    private BroadcastReceiver mMicReceiver;

    public static int uiBeaconIndex;

    public BluetoothAdapter mBluetoothAdapter;
    private LocationManager mLocationManager;

    // UI
    EditText input_name;
    ToggleButton tg_start;
    Spinner spn_mic_loc;
    Switch permit_rec;
    boolean canRec = true;

    // acc
    static float value_acc_x, value_acc_y, value_acc_z;
    static TextView tv_acc_x, tv_acc_y, tv_acc_z;

    // Beacon
    static String[] beaconValue;
    static LinearLayout ll_beacon1, ll_beacon2, ll_beacon3;
    static TextView tv_uuid, tv_major, tv_minor, tv_rssi;

    // step
    static int stpValue;
    static RelativeLayout rl_stp1;
    static TextView tv_stp;

    // 音圧レベル
    static double micValue;
    static  TextView tv_mic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // about permission for android 6.0
        String permission_array[] = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permission_array, 0);
        }

        InstanceForReceiver();
        InstanceForUI();

        // マイク接続位置について
        spn_mic_loc = (Spinner) findViewById(R.id.spn_mic_loc);
        spn_mic_loc.setOnItemSelectedListener(this);

        // Bluetooth Location 初期化
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        permit_rec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    canRec = true;
                }
                if(!isChecked){
                    canRec = false;
                }
            }
        });

        // トグルキーのイベントリスナー
        tg_start.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // トグルキーが変更された際に呼び出される
                if(isChecked){
                    if(ConfirmDevice()){
                        String text = input_name.getText().toString();
                        String str_mic_loc = spn_mic_loc.getSelectedItem().toString();

                        // バックグラウンドサービス呼び出し
                        Intent i = new Intent(MainActivity.this, BackService.class);
                        i.putExtra("userNum", text);
                        i.putExtra("str_mic_loc", str_mic_loc);
                        i.putExtra("canRec", canRec);
                        startService(i);

                        // 音圧------------------------------------//

                        // 音圧------------------------------------//
                    }
                }

                if(!isChecked){
                    stopService( new Intent( MainActivity.this, BackService.class ) );
                    Log.i("tg", "off");
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bcnLBM.unregisterReceiver(mAccReceiver);
    }

    public boolean ConfirmDevice(){
        // bluetoothがonになってるのか確認
        if (!mBluetoothAdapter.isEnabled()) {
            tg_start.setChecked(false);
            Toast t;
            t = Toast.makeText(MainActivity.this, "BluetoothをONにしてください", Toast.LENGTH_SHORT);
            t.show();
            return false;
        }
        // 位置情報がonになってるのか確認
        if(!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            tg_start.setChecked(false);
            Toast t;
            t = Toast.makeText(MainActivity.this, "位置情報をONにしてください", Toast.LENGTH_SHORT);
            t.show();
            return false;
        }

        // 被験者番号が書かれているかどうかの確認
        if(input_name.length() == 0){
            tg_start.setChecked(false);
            Toast t;
            t = Toast.makeText(MainActivity.this, "被験者番号を入れてください", Toast.LENGTH_SHORT);
            t.show();
            return false;
        }

        return true;
    }

    public void InstanceForUI(){
        // UI初期化
        input_name = (EditText) findViewById(R.id.input_name);
        tg_start = (ToggleButton) findViewById(R.id.tg_start);
        permit_rec = (Switch) findViewById(R.id.permit_rec);

        // beacon UI
        uiBeaconIndex = 0;
        beaconValue = new String[4];
        ll_beacon1 = (LinearLayout) findViewById(R.id.beacon1);
        ll_beacon2 = (LinearLayout) findViewById(R.id.beacon2);
        ll_beacon3 = (LinearLayout) findViewById(R.id.beacon3);
        tv_uuid = (TextView)findViewById(R.id.value_uuid_1);
        tv_major = (TextView)findViewById(R.id.value_major_1);
        tv_minor = (TextView)findViewById(R.id.value_minor_1);
        tv_rssi = (TextView)findViewById(R.id.value_rssi_1);

        // acc UI
        tv_acc_x = (TextView) findViewById(R.id.value_acc_x);
        tv_acc_y = (TextView) findViewById(R.id.value_acc_y);
        tv_acc_z = (TextView) findViewById(R.id.value_acc_z);

        // stp UI
        rl_stp1 = (RelativeLayout) findViewById(R.id.stp1);
        tv_stp = (TextView)findViewById(R.id.value_stp_1);

        // mic UI
        tv_mic = (TextView)findViewById(R.id.value_mic);

    }

    public void InstanceForReceiver(){
        // サービスから値を受け取るためのBroadcast ----------------------------//
        // Beaconの値受け取り
        bcnLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        mBcnReceiver = new BeaconReceiver();
        {
            final IntentFilter filter = new IntentFilter();
            filter.addAction("BEACON");
            bcnLBM.registerReceiver(mBcnReceiver, filter);
        }
        // acc dataの値受け取り
        accLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        mAccReceiver = new AccReceiver();
        {
            final IntentFilter filter = new IntentFilter();
            filter.addAction("ACC");
            accLBM.registerReceiver(mAccReceiver, filter);
        }
        // step dataの値受け取り
        stpLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        mStpReceiver = new StpReceiver();
        {
            final IntentFilter filter = new IntentFilter();
            filter.addAction("STP");
            stpLBM.registerReceiver(mStpReceiver, filter);
        }

        // mic dataの値受け取り
        micLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        mMicReceiver = new MicReceiver();
        {
            final IntentFilter filter = new IntentFilter();
            filter.addAction("MIC");
            micLBM.registerReceiver(mMicReceiver, filter);
        }

    }


    /** レシーバクラス */
    // beacon receiver class
    private static class BeaconReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            beaconValue = intent.getStringArrayExtra("beaconValue");
            ShowBeaconUI(beaconValue);

        }

        public void ShowBeaconUI(String value[]){
            LinearLayout parent = ll_beacon1;
            switch (uiBeaconIndex){
                case 0:
                    parent = ll_beacon1;
                    break;
                case 1:
                    parent = ll_beacon2;
                    break;
                case 2:
                    parent = ll_beacon3;
                    break;
            }

            tv_uuid = (TextView) parent.findViewWithTag("value_uuid");
            tv_major = (TextView) parent.findViewWithTag("value_major");
            tv_minor = (TextView) parent.findViewWithTag("value_minor");
            tv_rssi = (TextView) parent.findViewWithTag("value_rssi");

            tv_uuid.setText(value[0]);
            tv_major.setText(value[1]);
            tv_minor.setText(value[2]);
            tv_rssi.setText(value[3]);

            uiBeaconIndex = countUIIndex(uiBeaconIndex);
        }

    }

    // acc receiver class
    private static class AccReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            value_acc_x = intent.getFloatExtra("acc_x", 0);
            value_acc_y = intent.getFloatExtra("acc_y", 0);
            value_acc_z = intent.getFloatExtra("acc_z", 0);
            tv_acc_x.setText(String.valueOf(value_acc_x));
            tv_acc_y.setText(String.valueOf(value_acc_y));
            tv_acc_z.setText(String.valueOf(value_acc_z));
        }
    }

    // step receiver class
    private static class StpReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            stpValue = intent.getIntExtra("stpValue", 0);
            ShowStpUI(stpValue);
        }

        public void ShowStpUI(int value){
            RelativeLayout parent = rl_stp1;
            tv_stp = (TextView) parent.findViewWithTag("value_stp");
            tv_stp.setText(String.valueOf(value));
        }
    }

    // step receiver class
    private static class MicReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            micValue = intent.getDoubleExtra("micValue", 0);
            ShowStpUI(micValue);
        }

        public void ShowStpUI(double value){
            tv_mic.setText(String.valueOf(value));
        }
    }

    static int countUIIndex(int index){
        int count = index;
        if(count > 2){
            count = 0;
        }
        count += 1;
        return count;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }



}