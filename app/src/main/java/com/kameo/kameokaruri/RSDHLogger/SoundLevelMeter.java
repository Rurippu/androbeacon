package com.kameo.kameokaruri.RSDHLogger;

/**
 * Created by kameokaruri on 2017/01/12.
 */

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SoundLevelMeter implements Runnable {
    private static final int SAMPLE_RATE = 44100;

    private int bufferSize;
    private AudioRecord audioRecord;
    private boolean isRecording;
    private boolean isPausing;
    private double baseValue;

    private double micLev;
    private boolean canRec;

    //-----------------------
    private final int FILESIZE_SEEK = 4;
    private final int DATASIZE_SEEK = 40;
    private RandomAccessFile raf; //リアルタイム処理なのでランダムアクセスファイルクラスを使用する
    private File recFile; //録音後の書き込み、読み込みようファイル
    private String fileName = null; //録音ファイルのパス
    private byte[] RIFF = {'R','I','F','F'}; //wavファイルリフチャンクに書き込むチャンクID用
    private int fileSize = 36;
    private byte[] WAVE = {'W','A','V','E'}; //WAV形式でRIFFフォーマットを使用する
    private byte[] fmt = {'f','m','t',' '}; //fmtチャンク スペースも必要
    private int fmtSize = 16; //fmtチャンクのバイト数
    private byte[] fmtID = {1, 0}; // フォーマットID リニアPCMの場合01 00 2byte
    private short chCount = 1; //チャネルカウント モノラルなので1 ステレオなら2にする
    private int bytePerSec = SAMPLE_RATE * (fmtSize / 8) * chCount; //データ速度
    private short blockSize = (short) ((fmtSize / 8) * chCount); //ブロックサイズ (Byte/サンプリングレート * チャンネル数)
    private short bitPerSample = 16; //サンプルあたりのビット数 WAVでは8bitか16ビットが選べる
    private byte[] data = {'d', 'a', 't', 'a'}; //dataチャンク
    private int dataSize = 0; //波形データのバイト数
    //-----------------------
    private MediaRecorder rec;


    public interface SoundLevelMeterListener {
        void onMeasure(double db);
    }

    private SoundLevelMeterListener listener;

    public SoundLevelMeter() {
        bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT);
        audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC,
                SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT, bufferSize);
        listener = null;
        isRecording = true;
        baseValue = 12.0;
        pause();
    }

    public void setListener(SoundLevelMeterListener l) {
        listener = l;
    }

    public void SetCanRec(boolean bool){
        canRec = bool;
    }

    @Override
    public void run() {
        android.os.Process
                .setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        resume();

        short[] buffer = new short[bufferSize];
        while (isRecording) {
            if (!isPausing) {
                int read = audioRecord.read(buffer, 0, bufferSize);
                if (read < 0) {
                    throw new IllegalStateException();
                }

                int maxValue = 0;
                for (int i = 0; i < read; i++) {
                    maxValue = Math.max(maxValue, buffer[i]);
                }

                double db = 20.0 * Math.log10(maxValue / baseValue);
                //Log.d("SoundLevelMeter", "dB:" + db);

                // ---------------------------//
                int bufferSum = 0;
                float bufferAve;

                for (int i = 0; i < bufferSize; i++) {
                    bufferSum = bufferSum + Math.abs(buffer[i]);
                }
                bufferAve = bufferSum/(bufferSize);
                // ここからlogの処理 dBのマイクレベルに変換
                micLev = 20 * Math.log10(bufferAve / 32767);
                if(canRec){
                    addBigEndianData(buffer);
                }
                // ---------------------------//

                if (listener != null) {
                    listener.onMeasure(db);
                }
            }

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        audioRecord.stop();
        audioRecord.release();
    }

    public double MicLev(){
        return  micLev;
    }


    public void stop() {
        isRecording = false;

    }

    public void pause() {
        if (!isPausing)
            audioRecord.stop();
        isPausing = true;
    }

    public void resume() {
        if (isPausing) {
            audioRecord.startRecording();

        }
        isPausing = false;
    }

    public void setBaseValue(double value) {
        baseValue = value;
    }

    // -------------------------------------------------
    public	void createFile(String	fName){
        fileName = fName;
        //	ファイルを作成
        recFile	= new File(fileName);
        if(recFile.exists()){
            recFile.delete();
        }
        try	{
            recFile.createNewFile();
        }	catch	(IOException	e)	{
            //	TODO	Auto-generated	catch	block
            e.printStackTrace();
        }

        try	{
            raf	=	new	RandomAccessFile(recFile,	"rw");
        }	catch	(FileNotFoundException	e)	{
            //	TODO	Auto-generated	catch	block
            e.printStackTrace();
        }

        //wavのヘッダを書き込み
        try	{
            raf.seek(0);
            raf.write(RIFF);
            raf.write(littleEndianInteger(fileSize));
            raf.write(WAVE);
            raf.write(fmt);
            raf.write(littleEndianInteger(fmtSize));
            raf.write(fmtID);
            raf.write(littleEndianShort(chCount));
            raf.write(littleEndianInteger(SAMPLE_RATE)); //サンプリング周波数
            raf.write(littleEndianInteger(bytePerSec));
            raf.write(littleEndianShort(blockSize));
            raf.write(littleEndianShort(bitPerSample));
            raf.write(data);
            raf.write(littleEndianInteger(dataSize));

        }	catch	(IOException	e)	{
            //	TODO	Auto-generated	catch	block
            e.printStackTrace();
        }

    }

    private byte[] littleEndianInteger(int i){
        byte[] buffer = new byte[4];
        buffer[0] = (byte) i;
        buffer[1] = (byte) (i >> 8);
        buffer[2] = (byte) (i >> 16);
        buffer[3] = (byte) (i >> 24);

        return buffer;

    }

    // PCMデータを追記するメソッド
    public void addBigEndianData(short[] shortData){

        // ファイルにデータを追記
        try {
            raf.seek(raf.length());
            raf.write(littleEndianShorts(shortData));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        // ファイルサイズを更新
        updateFileSize();

        // データサイズを更新
        updateDataSize();

    }

    // ファイルサイズを更新
    private void updateFileSize(){

        fileSize = (int) (recFile.length() - 8);
        byte[] fileSizeBytes = littleEndianInteger(fileSize);
        try {
            raf.seek(FILESIZE_SEEK);
            raf.write(fileSizeBytes);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    // データサイズを更新
    private void updateDataSize(){

        dataSize = (int) (recFile.length() - 44);
        byte[] dataSizeBytes = littleEndianInteger(dataSize);
        try {
            raf.seek(DATASIZE_SEEK);
            raf.write(dataSizeBytes);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    // short型変数をリトルエンディアンのbyte配列に変更
    private byte[] littleEndianShort(short s){

        byte[] buffer = new byte[2];

        buffer[0] = (byte) s;
        buffer[1] = (byte) (s >> 8);

        return buffer;

    }

    // short型配列をリトルエンディアンのbyte配列に変更
    private byte[] littleEndianShorts(short[] s){

        byte[] buffer = new byte[s.length * 2];
        int i;

        for(i = 0; i < s.length; i++){
            buffer[2 * i] = (byte) s[i];
            buffer[2 * i + 1] = (byte) (s[i] >> 8);
        }
        return buffer;
    }


    // ファイルを閉じる
    public void close(){
        try {
            raf.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
    // -------------------------------------------------
}


