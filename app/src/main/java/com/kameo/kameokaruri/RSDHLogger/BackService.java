package com.kameo.kameokaruri.RSDHLogger;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.kameo.kameokaruri.RSDHLogger.SoundLevelMeter.SoundLevelMeterListener;

/**
 * Created by kameokaruri on 2016/12/15.
 */


public class BackService extends Service implements SensorEventListener ,SoundLevelMeterListener{
    public static final int INTERVAL_FILE_SAVE = 600; // ファイル更新の時間間隔[s]
    private String outputFile_Path = null; // ファイルの保存パス
    public String userNum = null; // 被験者番号
    public String str_mic_loc = null; // マイク位置
    public String[] mic_loc = new String[] {"上", "下"};
    public String fileName = null; // ファイル名
    public File fileDir;
    public boolean canRec = true;
    public int fileIndex = 0; // ファイルの番号
    public boolean Is_NewLocFile = true;
    public boolean Is_NewAccFile = true;
    public boolean Is_NewStpFile = true;
    public boolean Is_NewMicFile = true;

    // インターバル時間ごとに行うための、タイマーとハンドラー
    private Timer mTimer = null;
    Handler mHandler = new Handler();
    private static Toast t;

    // CSVデータの一行
    public String beaconData = null; //csv用のデータをつなげたもの
    public String accData = null;
    public String stepData = null;

    // センサー系 BLT, ACC, STP
    public BluetoothAdapter mBluetoothAdapter;
    private LocationManager mLocationManager;
    public SensorManager mSensorManager;
    private Sensor mSensorAcc;
    private Sensor mSensorStep;

    // 1秒ごとの歩数計測
    Handler stpHandler = new Handler();
    int stepCount = 0;
    private Timer stpTimer = null;

    // 音声録音
    //public MediaRecorder myMediaRecorder;

    // 音圧レベルの設定-----------------------------//
    // Sense Sound
    private SoundLevelMeter soundLevelMeter;
    private double maxSoundLevel = -1;

    ///-------------------------------------------////

    // UIのためのBroadcast
    private LocalBroadcastManager accLBM;
    private LocalBroadcastManager bcnLBM;
    private LocalBroadcastManager stpLBM;
    private LocalBroadcastManager micLBM;

    // UIのための
    float[] accValue;
    String[] beaconValue;

    @Override
    public void onCreate() {
        accLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        bcnLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        stpLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        micLBM = LocalBroadcastManager.getInstance(getApplicationContext());
        accValue = new float[3];
        beaconValue = new String[4]; // uuid, major, min, rssi

        ///-------------------------------------------////
        soundLevelMeter = new SoundLevelMeter();
        soundLevelMeter.setListener(this);
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(getApplicationContext());
        double baseValue = Double.valueOf(sp.getString("decibel_base_value", "12"));
        soundLevelMeter.setBaseValue(baseValue);
        (new Thread(soundLevelMeter)).start();

        ///-------------------------------------------////

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Bluetooth
        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        mBluetoothAdapter.startLeScan(mLeScanCallback);
        // 位置情報
        mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorAcc = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); // 加速度センサー
        mSensorStep = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR); // 歩数センサー
        if (mSensorAcc != null) {
            mSensorManager.registerListener(this, mSensorAcc, SensorManager.SENSOR_DELAY_GAME);
        }
        if (mSensorStep != null) {
            mSensorManager.registerListener(this, mSensorStep, 1000);
        }

        userNum = intent.getStringExtra("userNum");
        str_mic_loc = intent.getStringExtra("str_mic_loc");
        canRec = intent.getBooleanExtra("canRec", true);
        outputFile_Path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + getApplicationContext().getPackageName();
        fileName = userNum + "_" + GetDate() + "_" + fileIndex;
        fileDir = new File(outputFile_Path);
        fileDir.mkdirs();

        //startMediaRecord();

        // 音圧レベルの設定-----------------------------//
        soundLevelMeter.SetCanRec(canRec);
        if(canRec){
            soundLevelMeter.createFile(outputFile_Path + "/" + userNum +"_"+ GetDate() +"_"+ fileIndex + ".wav");
        }
        ///-------------------------------------------////

        // タイマーの設定
        mTimer = new Timer(true);
        mTimer.schedule( new TimerTask(){
            @Override
            public void run(){
                mHandler.post( new Runnable(){
                    public void run(){
                        Log.d( "TestService" , "Timer run" );
                        ToastCanceler(t);
                        t = Toast.makeText(BackService.this, "recording", Toast.LENGTH_LONG);
                        t.show();
                        fileIndex += 1;
                        if(canRec) {
                            soundLevelMeter.close();
                            soundLevelMeter.createFile(outputFile_Path + "/" + userNum + "_" + GetDate() + "_" + fileIndex + ".wav");
                        }
                        //startMediaRecord();

                        NewFile();

                    }
                });
            }
        }, 1000 * INTERVAL_FILE_SAVE, 1000 * INTERVAL_FILE_SAVE);


        // タイマーの設定
        stpTimer = new Timer(true);
        stpTimer.schedule( new TimerTask(){
            @Override
            public void run(){
                stpHandler.post( new Runnable(){
                    public void run(){
                        StepCounter();
                    }
                });
            }
        }, 1000, 1000);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i("TestService", "onDestroy");
        if(mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            mBluetoothAdapter = null;
        }
        if( mTimer != null ){
            mTimer.cancel();
            mTimer = null;
        }
        if( stpTimer != null ){
            stpTimer.cancel();
            stpTimer = null;
        }
        mSensorStep = null;

        //StopRecord();

        /// 音圧レベル --------------------------///
        soundLevelMeter.stop();
        if(canRec){
            soundLevelMeter.close();
        }

        /// 音圧レベル --------------------------///

        mSensorManager.unregisterListener(this);

        Toast.makeText(this, "バックグラウンド終了", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }



    /// 音圧レベル --------------------------------///
    @Override
    public void onMeasure(double db) {
        //Log.d("SoundLevelMeter", "dB:" + db);
        maxSoundLevel = Math.max(maxSoundLevel, db);
        double micLev = soundLevelMeter.MicLev();
        Log.d("SoundLevelMeter", "dB:" + micLev);
        String mic = GetDate() + "," + GetEpochTime() + "," + String.valueOf(micLev) + "\n";
        ExportCsv(mic, ".mic");
        SendMicReceiver(micLev);

        // On the way, update base value
        Context context = getApplicationContext();
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        double baseValue = Double.valueOf(sp.getString("decibel_base_value",
                "12"));
        soundLevelMeter.setBaseValue(baseValue);
    }


    /// 音圧レベル --------------------------///


    @Override
    public IBinder onBind(Intent arg0) {
        Log.i("TestService", "onBind");
        return null;
    }

//    // 録音
//    private void startMediaRecord(){
//        try {
//            if(fileIndex != 0) {
//                myMediaRecorder.stop();
//                myMediaRecorder.reset();
//                myMediaRecorder.release();
//                myMediaRecorder = null;
//            }
//            //レコード設定。これらはこの中で定義しないと実行毎に更新されない。
//            myMediaRecorder = new MediaRecorder();
//            myMediaRecorder.setOutputFile(outputFile_Path + "/" + userNum +"_"+ GetDate() +"_"+ fileIndex +".3gp");
//            myMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//            myMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//            myMediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
//            myMediaRecorder.prepare();
//            myMediaRecorder.start();
//
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        Log.d("audio","is recording");
//    }
//
//    private void StopRecord(){
//        if(myMediaRecorder == null){
//            Toast.makeText(BackService.this, "myAudioRecorder = null", Toast.LENGTH_SHORT).show();
//        }else{
//            try{
//                //録音停止
//                myMediaRecorder.stop();
//                myMediaRecorder.reset();
//                myMediaRecorder.release();
//                myMediaRecorder = null;
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accValue[0] = sensorEvent.values[0];
            accValue[1] = sensorEvent.values[1];
            accValue[2] = sensorEvent.values[2];
            accData = GetDate() + "," + GetEpochTime() + "," + accValue[0] + "," + AccByMicLoc(accValue[1]) + "," + accValue[2] + "\n";
            ExportCsv(accData, ".acc");
            final Intent intent = new Intent();
            intent.setAction("ACC");
            intent.putExtra("acc_x", accValue[0]);
            intent.putExtra("acc_y", accValue[1]);
            intent.putExtra("acc_z", accValue[2]);
            accLBM.sendBroadcast(intent);
        }

        // 歩数検知

        if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            //Log.d("type_step_counter",String.valueOf(values[0]));
            stepCount ++;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public float AccByMicLoc(float y){
        if(str_mic_loc.equals(mic_loc[0])){
            y = -y;
        }
        return y;
    }

    public void StepCounter(){
        stepData = GetDate() + "," + stepCount + "\n";
        ExportCsv(stepData, ".stp");
        SendStpReceiver(stepCount);
        stepCount = 0;
    }

    public void SendBeaconReceiver(String id, String maj, String min, String rss){
        final Intent intent = new Intent();
        intent.setAction("BEACON");
        beaconValue[0] = id;
        beaconValue[1] = maj;
        beaconValue[2] = min;
        beaconValue[3] = rss;
        intent.putExtra("beaconValue", beaconValue);
        bcnLBM.sendBroadcast(intent);

    }

    public void SendStpReceiver(int stp){
        final Intent intent = new Intent();
        intent.setAction("STP");
        intent.putExtra("stpValue", stp);
        stpLBM.sendBroadcast(intent);

    }

    public void SendMicReceiver(double mic){
        final Intent intent = new Intent();
        intent.setAction("MIC");
        intent.putExtra("micValue", mic);
        micLBM.sendBroadcast(intent);

    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            // デバイスが検出される度に呼び出されます。
            if (scanRecord.length > 30) {
                if ((scanRecord[5] == (byte) 0x4c) && (scanRecord[6] == (byte) 0x00) &&
                        (scanRecord[7] == (byte) 0x02) && (scanRecord[8] == (byte) 0x15)) {
                    String uuid = IntToHex2(scanRecord[9] & 0xff)
                            + IntToHex2(scanRecord[10] & 0xff)
                            + IntToHex2(scanRecord[11] & 0xff)
                            + IntToHex2(scanRecord[12] & 0xff)
                            + "-"
                            + IntToHex2(scanRecord[13] & 0xff)
                            + IntToHex2(scanRecord[14] & 0xff)
                            + "-"
                            + IntToHex2(scanRecord[15] & 0xff)
                            + IntToHex2(scanRecord[16] & 0xff)
                            + "-"
                            + IntToHex2(scanRecord[17] & 0xff)
                            + IntToHex2(scanRecord[18] & 0xff)
                            + "-"
                            + IntToHex2(scanRecord[19] & 0xff)
                            + IntToHex2(scanRecord[20] & 0xff)
                            + IntToHex2(scanRecord[21] & 0xff)
                            + IntToHex2(scanRecord[22] & 0xff)
                            + IntToHex2(scanRecord[23] & 0xff)
                            + IntToHex2(scanRecord[24] & 0xff);

                    String major = String.valueOf(scanRecord[25] & 0xff) +String.valueOf(scanRecord[26] & 0xff);
                    String minor = String.valueOf(scanRecord[27] & 0xff) + String.valueOf(scanRecord[28] & 0xff);
                    String rssi_str = String.valueOf(rssi);
                    beaconData = FormatBeaconInfo(major, minor, rssi_str);
                    ExportCsv(beaconData, ".loc");
                    SendBeaconReceiver(uuid, major, minor, rssi_str);

                }
            }
        }
    };

    public String FormatBeaconInfo(String maj, String min, String rssi){
        String dataString = GetDate() + "," + GetEpochTime() + "," + maj + "," + min + "," + rssi + "\n";
        return dataString;
    }


    // その他諸々の関数
    public void NewFile(){
        fileName = userNum + "_" + GetDate() + "_" + fileIndex;
        File directory = new File(outputFile_Path);
        directory.mkdirs();
        Is_NewLocFile = true;
        Is_NewAccFile = true;
        Is_NewStpFile = true;
        Is_NewMicFile = true;

    }

    public String GetEpochTime(){
        Long tsLong = System.currentTimeMillis();
        String ts = String.valueOf(tsLong);

        return ts;
    }

    public String GetDate(){
        Date now = new Date(System.currentTimeMillis());
        java.text.DateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
        // フォーマット
        String nowText = formatter.format(now);
        return nowText;
    }

    public void ExportCsv(String data, String fileExt) {
        String outputData = data;
        FileOutputStream outputStream;

        try {
            outputStream = new FileOutputStream(outputFile_Path + "/" + fileName + fileExt, true);

            String headerCSV;
            switch (fileExt){
                case ".loc":
                    if(Is_NewLocFile == true){
                        headerCSV = "date,epoch_time,major,minor,rssi"+"\n";
                        outputStream.write(headerCSV.getBytes());
                        Is_NewLocFile = false;
                    }
                    break;
                case ".acc":
                    if(Is_NewAccFile == true) {
                        headerCSV = "date,epoch_time,acc_x,acc_y,acc_z" + "\n";
                        outputStream.write(headerCSV.getBytes());
                        Is_NewAccFile = false;
                    }
                    break;
                case ".stp":
                    if(Is_NewStpFile == true) {
                        headerCSV = "date,steps" + "\n";
                        outputStream.write(headerCSV.getBytes());
                        Is_NewStpFile = false;
                    }
                    break;
                case ".mic":
                    if(Is_NewMicFile == true) {
                        headerCSV = "date,epoch_time,PeakPower" + "\n";
                        outputStream.write(headerCSV.getBytes());
                        Is_NewMicFile = false;
                    }
                    break;
                default:
                    headerCSV = null;
            }

            outputStream.write(outputData.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ToastCanceler(Toast toast){
        if(toast != null) {
            toast.cancel();
        }
    }


    public String IntToHex2(int i) {
        char hex_2[] = {Character.forDigit((i>>4) & 0x0f,16),Character.forDigit(i&0x0f, 16)};
        String hex_2_str = new String(hex_2);
        return hex_2_str.toUpperCase();
    }



}